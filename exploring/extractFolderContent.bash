#!/bin/bash

# Script para extraer el contenido del directorio actual destruye el directorio
#
# Para que este script funcione copiarlo a al archivo de alias y crear su
# respectivo alias
#
# Author: David Giordana 2017

path="$(cd ..; pwd)"
current=$(pwd)
for item in *; do
	mv "$item" "$path/"
done
cd ..
rm -rf "$current"

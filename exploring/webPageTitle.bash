# Obtiene una el título de una página web en base a una url
#
# Creado por David Giordana 2017

if [ $# -eq 0 ]; then
    echo "Este script retorna la el título de una página web, debe ingresar una ruta junto al comando para poder trabajar"
else
    wget -qO- $1 |
    perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si'
fi

#!/bin/bash

# Script para ingresar en un directorio en base a su índice
# Si se le pasa un argumento  al script se lo considerará como índice.
# En caso contrario se imprimirá la lista de archivos con su respectivo índice
#
# Para que esto funcione copiar al archivo de alias y crear un alias
#
# Author: David Giordana 2017

content=(*)
if [[ $# -eq 0 ]]; then
    for i in $(seq 0 $((${#content[@]}-1))); do
        echo "$((i+1))) ${content[$i]}"
    done
else
    cd "${content[$(($1-1))]}"
fi

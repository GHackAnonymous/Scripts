from os import listdir, mkdir, rename
from os.path import join, isdir, isfile, exists, basename, dirname, splitext
from shutil import move
import sys

# Script para agrupar los elementos de paginas web en un solo directorio
# Para hacer el trabajo este toma como argumento la ruta de directorio a trabajar
#
# Created by David Giordana 2017

# Obtiene la lista de archivos en un directorio, lista vacia en caso contrario
def getFilesIn(path):
    if not isdir(path):
        return []
    files = listdir(path)
    return map(lambda x: join(path, x), files)

# Obtiene los directorios en un directorio
def getFoldersPathIn(path):
    files = getFilesIn(path)
    return [x for x in files if isdir(x)]

# Obtiene los archivos HTML en un directorio
def getHtmlfiles(path):
    files = getFilesIn(path)
    return [x for x in files if x.endswith(".html") and isfile(x)]

# Agrega una lista de archivos a un directorio
# @param newFolderPath Ruta del directorio a crear
# @param items Lista de archivos a agregar
def embedInFolder(newFolderPath, items):
    ext = ".ws"
    # Genera el nombre del nuevo directorio (se asegura que no se repita)
    pf = newFolderPath + ext
    if exists(pf):
        i = 0
        while exists(pf):
            pf = newFolderPath + " - " + str(i) + ext
            i += 1
    mkdir(pf, mode=0o777)
    for item in items:
        move(item, pf)

# Obtiene los archivos asociados a un archivo HTML
def sourcesForHtml(htmlPath, folders):
    name = splitext(htmlPath)[0]
    return [x for x in folders if x.startswith(name)]

# Dada una ruta de un directorio empaqueta los archivos de pagina web
def embedWebSoruceInFolder(path):
    html = getHtmlfiles(path)
    folders = getFoldersPathIn(path)
    for item in html:
        sources = sourcesForHtml(item, folders)
        if len(sources) != 0:
            folders = [x for x in folders if not x in sources]
            sources.append(item)
            newFolderPath = join(path, splitext(item)[0])
            embedInFolder(newFolderPath, sources)
    for item in folders:
        if not item.endswith(".ws"):
            embedWebSoruceInFolder(item)

args = sys.argv[1:]
for folder in args:
    embedWebSoruceInFolder(folder)

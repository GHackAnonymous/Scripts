from os import listdir, mkdir
from os.path import join, isdir, exists
from shutil import move
import argparse

# Script para organizar el contenido de un directiorio
# Empaqueta el contenido en cada subdirectorio
# El empaquetado agrega todos los items a un nuevo directorio pero excluye una extensión (pasada por argumento)
#
# Created by David Giordana 2017

# Obtiene la lista de archivos en una ruta (incluye directorios)
def getFilesIn(path):
    if not isdir(path):
        return []
    files = listdir(path)
    return map(lambda x: join(path, x), files)

# Obtiene la lista de directorios en una ruta
def getFoldersPathIn(path):
    files = getFilesIn(path)
    return [x for x in files if isdir(x)]

# Obtiene el nombre de una carpeta de manera tal que esta no se solape con otra
def getFolderName(path):
    if not exists(path):
        return path
    i = 1
    while True:
        p = path + " - " + str(i)
        if not exists(p):
            return p
        i += 1

# Aplica el empaquetamiento en un directorio
# @param folder Directirio a aplicar la acción
# @param excludext Extensión a excluir
# @param srcname Nombre de la careta de recursos
def applyEmbed(folder, excludext, srcname):
    files = getFilesIn(folder)
    toMove = [x for x in files if not x.endswith("." + excludext)]
    srcFoler = getFolderName(join(folder, srcname))
    mkdir(srcFoler, mode=0o777)
    for item in toMove:
        move(item, srcFoler)


def main():
    #Parsea los argumentos
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--excludext", required=True, type=str, help="Extensión a ignorar en la agruoación")
    parser.add_argument("-d", "--directory", required=True, type=str, help="Ruta del directiorio a trabajar")
    parser.add_argument("-s", "--srcname", type=str, default="src", help="Nombre del directorio de recursos")
    args = parser.parse_args()
    # Si la ruta no es un directorio: no hay que trabajar
    if not isdir(args.directory):
        print("Error: La ruta debe ser un directorio")
        return
    folders = getFoldersPathIn(args.directory)
    for folder in folders:
        applyEmbed(folder, args.excludext, args.srcname)


main()

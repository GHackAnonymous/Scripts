#!/bin/bash

# Script para incluir una lista de items en un directorio
# Este programa toma el nombre del nuevo directorio donde se almacenarám
# para hacerlo utilizar la bandera -n o --name
#
# Author: David Giordana 2017

itemsToInclude=()               # Arreglo con los items a agregar al directorio
currentDirectory=$(pwd)         # Ruta del directorio actual
currentDirectoryContent=(*)     # Arreglo con los items del directorio actual

# Imprime la lista de archivos en el directorio actual
function printList {
    count=$((${#currentDirectoryContent[@]}-1))
    for i in $(seq 0 $count)
    do
       echo "$(($i+1))): ${currentDirectoryContent[$i]}"
    done
}

# Lee numeros individuales y secuencias de la forma i-j representando
# la secuencia i...j, y los mueve al nuevo directorio
function readNumbers {
    numbers=($(sed "s/-/ /" <<< "${itemsToInclude[$1]}"))
    if [[ ${#numbers[@]} -ge 1 ]]; then
        i1=${numbers[0]}
        if [[ ${#numbers[@]} -eq 1 ]]; then
            i2=${numbers[0]}
        else
            i2=${numbers[1]}
        fi
        for i in $(seq $i1 $i2); do
            index=$(($i-1))
            file="${currentDirectoryContent[$index]}"
            mv "$file" "$name/"
        done
    fi
}


if [[ $# -eq 0 ]]; then
    echo "Se requiere de un argumento para poder crear un directorio"
elif [[ ${#currentDirectoryContent[@]} -eq 0 ]]; then
    echo "El directorio está vacío"
else
    while [[ $# -gt 0 ]]; do
        key="$1"; shift
        case $key in
            -l|--list)
                list="t"
                ;;
            -n|--name)
                name="$1"; shift
                ;;
            *)
                itemsToInclude[${#itemsToInclude[@]}]="$key"
                ;;
        esac
    done
    if [[ "$list" == "t" ]]; then
        printList
    elif [[ -z "$name" ]]; then
        echo "ERROR: No se especificó nombre del directorio a crear"
    else
        if [[ ! -d "$name" ]]; then
            mkdir "$name"
        fi
        count=$((${#itemsToInclude[@]}-1))
        for i in $(seq 0 $count); do
            readNumbers $i
        done
    fi
fi

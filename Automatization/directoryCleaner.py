import argparse
import os
import os.path

# Script para acelerar el renombre y limpueza de directorios.
# La aplicación es recursiva.
# Las opreaciones soportadas son:
# * Eliminar archivos (solo archivos) que contienen cierto componente en su nombre
# * Renombrar archivos para quitar cierta componente. EJ: "archivo[extra]" reeplazando [extra] obtenemos el nuevo nombre "archivo"

deleteFile = []       # Nombres de archivos eliminables
removeFromName = []   # Nombres de archivos a renombrar

def main():
    global deleteFile
    global removeFromName
    #Parsea los argumentos
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--source", required=True, type=str, help="Directorio de trabajo")
    parser.add_argument("-d", "--deleteFile", default=None, type=str, help="Elimina un tipo de archivo")
    parser.add_argument("-r", "--removeFromName", default=None, type=str, help="Elimina parte de nombres")
    parser.add_argument("-f", "--fileCommands", default=None, type=str, help="Aplica comandos desde un archivo")
    args = parser.parse_args()

    #Aplica las acciones sobre el archivo pasado como argumento
    delf = args.deleteFile
    rem = args.removeFromName
    if delf != None:
        deleteFile.append(delf)
    if rem != None:
        removeFromName.append(rem)
    parseFile(args.fileCommands)
    applyAction(args.source)

# Parsea un archivo para ejecutar el script por lotes
def parseFile(path):
    global deleteFile
    global removeFromName
    lines = []
    try:
        f = open(path, "r")
        lines = f.readlines()
        f.close()
    except:
        pass
    for line in lines:
        splited = line.strip().split(" ", 1)
        if len(splited) > 1:
            (command, text) = splited
            if command == "-d":
                deleteFile.append(text)
            if command == "-r":
                removeFromName.append(text)


# Aplica la acción sobre una Ruta
def applyAction(src):
    global deleteFile
    global removeFromName
    # Obtiene los archivos luego del renombre
    files = getRenames(removeFromName, getFilesIn(src))
    for item in files:
        # Aplica la recursión en el directorio
        if os.path.isdir(item):
            applyAction(item)
        # Renombra un archivo si es necesario
        elif os.path.isfile(item):
            deleteIfNeeded(deleteFile, item)

# Obtiene los nombres de los archivos luego de hacer los renombres necesarios.
# Toma la lista de matches y la lista de archivos a aplicar las acciones
def getRenames(subStr, files):
    files = list(files)
    result = []
    for file in files:
        for sub in subStr:
            if sub in file:
                newName = file.replace(sub, "")
                print("Renombrando \"" + file + "\"")
                os.rename(file, newName)
                result.append(newName)
                files.remove(file)
                break
    return result + files

# Elimina el archivo si es necesario
def deleteIfNeeded(subStr, file):
    for sub in subStr:
        if sub in file:
            os.remove(file)
            print("Eliminando \"" + file + "\"")
            return


# Obtiene la lista de archivos en una ruta (incluye directorios)
def getFilesIn(path):
    if not os.path.isdir(path):
        return []
    files = os.listdir(path)
    return map(lambda x: os.path.join(path, x), files)

main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Script para renombrar una lista de archivos en forma de secuencia
#
# Entre los argumentos que acepta este script tenemos
# --step <valor> Representa el salto entre los numeros de los archivos
# --init | -i <valor> Representa el numerp inicial de la sucesion de archivos
# --sep <valor> Representa el separador entre el nuevo nombre y el número
# --name | -n Representa el nuevo nombre a asignar
#
# Cualquier otro valor debe ser un indice de la lista de archivos. Estos pueden
# ser numeros simples o rangos i-j (Ambos valores incluidos).
# El primer argumento debe ser la ruta del directorio a trabajar
#
# Author: David Giordana 2017

import sys
import os
import os.path

# Vañpres utilizados para asignar nuevo nombre
step = 1
init = 1
separator = " - "
name = ""

# Datos de los archivos a trabajar
currentDirectory = ""               # Ruta del directorio de trabajo
currentDirectoryContent = []        # Contenido del directorio de trabajo
itemsToInclude = []                 # Lista de archivos a renombrar

# Variables de control
toList = False  # True indica que solo debe mostrarse la lista de archivos
error = False   # True indica que ocurrió un error

# Lee los argumentos del script
def parseArguments():
    global error
    global currentDirectory
    global currentDirectoryContent
    global toList
    global init
    global step
    global separator
    global name

    args = sys.argv
    args.pop(0)
    if len(args) == 0:
        error = True
        print("ERROR: No se ha ingresado la ruta del directorio sobre la cual se trabajará")
        return
    currentDirectory = args.pop(0)
    fillCurrentDirContent()
    if len(currentDirectoryContent) == 0:
        error = True
        return
    while len(args) > 0:
        arg = args.pop(0)
        if arg.startswith("-"):
            if len(args) > 0:
                if (arg == "-n" or arg == "--name"):
                    name = args.pop(0)
                elif (arg == "-i" or arg == "--init"):
                    init = int(args.pop(0))
                elif (arg == "--step"):
                    step = int(args.pop(0))
                elif (arg == "--sep"):
                    separator = args.pop(0)
            elif arg == "-l" or arg == "--list":
                toList = True
        else:
            readNumbers(arg)

# Rellena la lista de archivos a trabajar
def fillCurrentDirContent():
    global currentDirectory
    global currentDirectoryContent
    files = os.listdir(currentDirectory)
    currentDirectoryContent = list(filter(lambda x: not x.startswith("."), files))

# Imprime la lista de archivos en el directorio actual
def printList():
    global currentDirectoryContent
    for i in range(len(currentDirectoryContent)):
       print("{0}): {1}".format(i+1, currentDirectoryContent[i]))


# Lee numeros individuales y secuencias de la forma i-j representando
# la secuencia i...j
def readNumbers(str):
    global currentDirectoryContent
    global itemsToInclude
    numbers = str.split("-")
    if len(numbers) == 1:
        index = int(numbers[0]) - 1
        element = currentDirectoryContent[index]
        itemsToInclude.append(element)
    elif len(numbers) >= 2:
        i1 = int(numbers[0])
        i2 = int(numbers[1])
        for i in range(i1, i2 + 1):
            index = i - 1
            element = currentDirectoryContent[index]
            itemsToInclude.append(element)

# Dado un índice y la cantidad de dígitos calcula en nuevo nombre
def getNewName(index, digits):
    global name
    global separator
    global step
    global init
    number = str(step * index + init)
    return "{0}{1}{2}".format(name,separator,number.zfill(digits))

# Dado el nombre de archivo base y un nuevo nombre renombra el archivo
def renameKeepingExtension(f, newName):
    global currentDirectory
    _, extension = os.path.splitext(f)
    oldPath = os.path.join(currentDirectory, f)
    newPath = os.path.join(currentDirectory, newName + extension)
    print(oldPath + "|" + newPath)
    #os.rename(oldPath, newPath)

if __name__ == "__main__":
    parseArguments()
    if toList:
        printList()
    elif name == "":
        print("ERROR: El nuevo nombre no fue asignado")
    elif len(itemsToInclude) == 0:
        print("ERROR: No se ha seleccionado ningún índice")
    else:
        count = len(itemsToInclude)
        maxNumber = init + count * step
        digits = len(str(maxNumber))
        for i in range(count):
            f = itemsToInclude[i]
            newn = getNewName(i, digits)
            renameKeepingExtension(f, newn)

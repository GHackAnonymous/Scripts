# Script para instalar Swift sencillamente
#
# Author: David Giordana 2017

swiftDirectory="$HOME/.swift"                   # Ruta del directorio temporal de trabajo
swiftContainer="$swiftDirectory/scontainer"     # Ruta del PATH de swift

# Arreglo con las versiones disponibles
declare -a sourceContent
sourceContent=(
"3.1.1 16.10"
"3.1.1 16.04"
"3.1.1 14.04"
"3.1 16.10"
"3.1 16.04"
"3.1 14.04"
"3.0.2 16.04"
"3.0.2 14.04"
"3.0.1 16.04"
"3.0.1 15.10"
"3.0.1 14.04"
"3.0 15.10"
"3.0 14.04"
"2.2.1 15.10"
"2.2.1 14.04"
"2.2 15.10"
"2.2 14.04")

# Imprime el saludo de bienvenida
function printWelcome {
    clear
    echo "***************************************************"
    echo "*  Bienvenido al script de instalación de Swift   *"
    echo "*                                                 *"
    echo "*         Creado por David Giordana 2017          *"
    echo "***************************************************"
}

##################################################################

# Crea los directorios de trabajo
function createEnviroment {
    [[ ! -d "$swiftDirectory" ]] && mkdir "$swiftDirectory"
    for t in "$swiftDirectory/*.tar.gz"; do
        rm -rf $t
    done
}

##################################################################

# Genera el nombre del archivo a descargar (sin extensión)
# Toma la version de swift y la versión de ubuntu
function generateFilename {
    sv=$1
    uv=$2
    echo "swift-$sv-RELEASE-ubuntu$uv"
}

# Genera la url del archivo a descargar. Toma la version de swift y luego la de ubuntu
# Toma la version de swift y la versión de ubuntu
function generateDownloadUrl {
    sv=$1
    uv=$2
    uvc=$(sed "s/\.//g" <<< $uv)
    echo "https://swift.org/builds/swift-$sv-release/ubuntu$uvc/swift-$sv-RELEASE/$(generateFilename $1 $2).tar.gz"
}

#Lista las versiones disponibles.
function listVersions {
    clear
    echo "Versión del sistema: $(lsb_release -s -d)"
    echo ""
    echo "Versiones disponibles:"
    for index in $(seq 0 $((${#sourceContent[@]}-1))); do
        data=(${sourceContent[$index]})
        echo "$(($index+1))) Swift ${data[0]} - Ubuntu ${data[1]}"
    done
}

# Instala las dependencias
function installDependencies {
    echo "Instalando Dependencias"
    if [[ -f "/var/cache/apt/archives/lock" ]] || [[ -d "/var/cache/apt/archives/lock" ]]; then
        sudo rm -rf /var/cache/apt/archives/lock
    fi
    if [[ -f "/var/lib/dpkg/lock" ]] || [[ -d "/var/lib/dpkg/lock" ]]; then
        sudo rm -rf /var/lib/dpkg/lock
    fi
    if [[ -f "/var/lib/apt/lists/lock" ]] || [[ -d "/var/lib/apt/lists/lock" ]]; then
        sudo rm -rf /var/lib/apt/lists/lock
    fi
    sudo apt-get --yes install clang libicu-dev
}

# Extrae el contenido del paquete de instalación
# Toma la ruta del archivo tar
function unpackData {
    [[ -d "$swiftContainer" ]] && rm -rf "$swiftContainer"
    mkdir "$swiftContainer"
    tar xf "$1" -C "$swiftContainer" --strip-components 1
    sudo chmod -R 755 "$swiftDirectory"
}

# Agrega la compilación de Swift al PATH
function addToPath {
    swiftPath="$swiftContainer/usr/bin"
    if [[ -f ~/.profile ]]; then
        echo "export PATH=\$PATH:$swiftPath" >> ~/.profile
        export PATH="$PATH:$swiftPath"
        source ~/.profile
    elif [[ -f ~/.bash_profile ]]; then
        echo "export PATH=\$PATH:$swiftPath" >> ~/.bash_profile
        export PATH="$PATH:$swiftPath"
        source ~/.bash_profile
    else
        echo "Error: No se pudo agregar swift al PATH."
        echo "La ruta a agregar al PATH es: \"$(swiftPath)\""
    fi
}

# Dada la version de swift y la de ubunto descarga e instala swift
function installPackage {
    url=$(generateDownloadUrl $1 $2)
    installDependencies
    echo "Instalando Swift"
    cd "$swiftDirectory"
    wget "$url"
    filename="$(generateFilename $1 $2).tar.gz"
    unpackData "$filename"
    addToPath
    read -p "Presiones enter para continuar " selected
}

# Instala swift
function install {
    listVersions
    read -p "Seleccione una opción: " selected
    versionsCount=${#sourceContent[@]}
    if [[ $selected -gt $versionsCount ]] || [[ 0 -ge $selected ]]; then
        echo "La opción $selected no existe, vuelva a intentarlo"
        sleep 3
        install
    fi
    data=(${sourceContent[$(($selected-1))]})
    installPackage ${data[0]} ${data[1]}
}

##################################################################

# Solicita alguna acción al usuario y la ejecuta
function runCommand {
    echo "***************************************************"
    echo "*    Seleccione una de las siguentes opciones     *"
    echo "***************************************************"
    echo "* 1) Listar versiones disponibles.                *"
    echo "* 2) Instalar                                     *"
    echo "* 3) Salir                                        *"
    echo "***************************************************"
    echo "*> Versión del sistema: $(lsb_release -s -d)"
    read -p "*> Seleccione una opción: " index
    case $index in
        1)
            listVersions
            read -p "Presione enter para continuar " trash
            ;;
        2)
            install
            ;;
        3)
            echo "Muchas gracias por usar el instalador de Swift."
            sleep 1
            exit 0
            ;;
        *)
            echo "La opción \"$Index\" no existe."
            sleep 3
    esac
}

##################################################################

# Prepara el entorno de trabajo
createEnviroment

# Bucle de trabajo
while true; do
    printWelcome
    runCommand
    clear
done

# -*- coding: utf-8 -*-
import argparse
from random import randint
from shutil import copyfile
import os.path

def main():
    # Prepara el parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--count", default=500, type=int, help="Cantidad de elementos en la secuencia")
    parser.add_argument("-d", "--destination", default="", type=str, help="Directorio donde se almacenará el archivo generado")
    parser.add_argument("-n", "--name", default="randomSwift", type=str, help="Nombre del archivo (sin extención) que se generará")

    # Ejecuta el script
    args = parser.parse_args()
    f = initFile(args.destination, args.name)
    randomCode = generateRandomSequence(args.count) + "}"
    f.write(randomCode)
    f.close()

# Comienza la operación del script
#
# Genera una copia del archivo base
def initFile(destination, name):
    path = os.path.join(destination, name + ".rtf")
    copyfile("base", path)
    return open(path, "a")

# Genera la secuencia aleatoria
def generateRandomSequence(count):
    f = open("swiftCharacters", "r")
    items = [ x.strip() for x in f.readlines() ]
    f.close()
    result = ""
    maxlen = len(items) - 1
    for i in range(count):
        result += items[randint(0, maxlen)] + getColorCode(i)
    return result

# Retorna un string que representa un código de color
def getColorCode(i):
    # Los códigos van del 3 al 7
    return "\cf" + str(3 + i % 5)

main()

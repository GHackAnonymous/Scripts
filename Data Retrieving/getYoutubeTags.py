import requests
from bs4 import BeautifulSoup
import argparse

# Script para obtener las etiquetas de un video de youtube
# dada su url.

def main():
    # Genera el parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", required=True, type=str, help="Url de video a analizar")

    # Aplica el script en la url obtenida
    args = parser.parse_args()
    getTags(args.url)

# Parsea el contenido de un sitio en base a una url
def parseUrl(url):
    # Obtiene la url completa
    if not url.startswith("http://") and not url.startswith("https://"):
        url = "http://" + url
    session = requests.Session()
    resp = session.head(url, allow_redirects=True)

    # Obtiene el contenido de la url
    r = requests.get(resp.url)
    data = r.content
    return BeautifulSoup(data, "html.parser")


# Obitne las etiquetas de un video de YouTube
def getTags(url):
    data = parseUrl(url)
    # Procesa la informaci+on
    metas = data.findAll("meta", {"property": "og:video:tag"})
    for meta in metas:
        print(meta["content"])

main()

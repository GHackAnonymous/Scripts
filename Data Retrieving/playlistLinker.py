import requests
from bs4 import BeautifulSoup
import argparse

# Script para obtener la lista de encadenamientos de una
# lista de reproducción de youtube

def main():
    # Prepara el parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", required=True, type=str, help="Url de una lista de reporducción de youtube")
    parser.add_argument("-r", "--reversed", default=False, type=bool, help="Indica si hay que revertir el orden de los elementos")
    # Ejecuta el script
    args = parser.parse_args()
    getArticles(args.url, args.reversed)

# Parsea el contenido de un sitio en base a una url
def parseUrl(url):
    # Obtiene la url completa
    if not url.startswith("http://") and not url.startswith("https://"):
        url = "http://" + url
    session = requests.Session()
    resp = session.head(url, allow_redirects=True)

    # Obtiene el contenido de la url
    r = requests.get(resp.url)
    data = r.content
    return BeautifulSoup(data, "html.parser")

# Obtiene los items de la lista de reproducción y los procesa
def getArticles(url, reversed=False):
    data = parseUrl(url)

    # Filtra el contenido del sitio
    table = data.find("table", {"class": "pl-video-table"})
    articles = table.findAll("tr")
    if reversed:
        articles.reverse()
    articleData = [getArticleInfo(x) for x in articles]
    pointersData = [(getElementSecure(i - 1, articleData), articleData[i], getElementSecure(i + 1, articleData)) for i in range(len(articleData))]

    # Imprime la informaci+on procesada
    for prev, (title,id), next in pointersData:
        print("video: {0}".format(title))
        print("https://www.youtube.com/edit?video_id={0}".format(id))

        if prev != None:
            print("prev:")
            print("https://www.youtube.com/watch?v=" + prev[1])

        if next != None:
            print("next:")
            print("https://www.youtube.com/watch?v=" + next[1])

        print("")

# Convierte un item de lista en (titulo, id de video)
def getArticleInfo(article):
    title = article["data-title"]
    id = article["data-video-id"]
    return (title, id)

# Obtiene un elemento de una lista siempre que se pueda
def getElementSecure(index, list):
    if index < 0 or index >= len(list):
        return None
    return list[index]

main()
